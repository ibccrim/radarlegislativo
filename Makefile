build:
	sudo docker-compose build
	sudo docker-compose run --rm web python manage.py migrate --noinput
	sudo docker-compose run --rm web python manage.py loaddata main/fixtures/tags.json
	sudo docker-compose run --rm web python manage.py rebuild_index --noinput

deploy: build
	sudo docker-compose up -d

stop:
	sudo docker-compose stop

update:
	sudo docker-compose run --rm web python manage.py update_all_projetos

update_index:
	sudo docker-compose run --rm web python manage.py update_index

tramitabot_send_updates:
	sudo docker-compose run --rm tramitabot python manage.py tramitabot_send_updates

dev_server:
	cd radarlegislativo && python manage.py runserver

dev_celery:
	cd radarlegislativo && celery -A radarlegislativo worker -l info --concurrency=1

dev_init_db:
	cd radarlegislativo && python manage.py migrate
	cd radarlegislativo && python manage.py loaddata main/fixtures/tags.json \
											main/fixtures/projetos.json \
											main/fixtures/tramitacoes.json \
											parlamento/fixtures/comissoes.json

dev:
	./scripts/run_dev.sh

compile_css:
	cd radarlegislativo/website/static/styles/ && lessc radar.less radar.css
