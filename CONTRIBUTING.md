Notas de desenvolvimento
========================

(for English version, see [contributing-en][contributing-en]).

Este documento descreve a configuração básica do ambiente de desenvolvimento.
Ele descreve como rodar o projeto com dados de exemplo. Para configurar o
processo de captura dos projetos de lei, por favor leia
[`docs/captura.md`][captura].

Pré-requisitos
--------------

### Instalação local (sem Docker)

Você deve ter o Python instalado na máquina, e o instalador de pacotes pip.
Em sistemas GNU/Linux, ambos podem ser encontrados nos repositórios
(``python`` e ``python-pip``).

Recomendamos usar o virtualenv para facilitar o gerenciamento dos pacotes
python. Com o [virtualenv e o virtualenvwrapper instalados][guia-virtualenv],
basta criar o virtualenv:

```
$ mkvirtualenv radarlegislativo
```

Em seguida, instale os requerimentos do terminal estando dentro do diretório do
repositório (o mesmo deste arquivo) e com o virtualenv ativado:

```
$ pip install --upgrade -r requirements-dev.txt
```

É necessário também que o [elasticsearch 2.4][elasticsearch] esteja instalado.

Para sistemas Debian ou Ubuntu GNU/Linux, basta rodar o script incluído
nesse repositório:

```
$ ./scripts/get_elasticsearch.sh
```

Esse script fará o download do elasticsearch e o deixará disponível no lugar
esperado pelos outros scripts deste repositório.

Caso você não use uma dessas distribuições linux, siga as [instruções de
instalação][elastic-install].

É necessário também instalar o compilador [LESS][less-css] e o
rabbitmq. Nos sistemas Debian ou Ubuntu GNU/Linux, basta usar o
gerenciador de pacotes e o npm:

```
$ sudo apt install npm rabbitmq-server
$ sudo npm install -g less
```

### Instalação local (sem Docker)

Você vai precisar do [Docker][docker] e [Docker Compose][docker-compose] instalados:

```
$ docker-compose -f docker-compose.dev.yml build
```

Desenvolvimento
---------------

Crie, no diretório raiz do projeto, um arquivo de configuração específico do
ambiente (você pode usar o exemplo inclúido como base):

```
$ cp example-env .env
```

Modifique o arquivo para que a linha que contém `DEBUG=False` fique
com `DEBUG=True`.

No arquivo `.env`, além de definir a variável `DEBUG` você pode
definir várias outras configurações. Por exemplo, o conteúdo de
`SECRET_KEY` deve ser único para cada instalação e mantido em segredo
por [questões de segurança][django_secret_key].

Para que o tramitabot funcione, você precisa definir neste mesmo
arquivo (`.env`) a variável `TRAMITABOT_API_TOKEN`, com o valor
fornecido no [processo de criação do seu bot][telegram_bot]. Se você não
precisa do tramitabot funcionando agora, você pode definir qualquer valor para
essa variável por enquanto.

Outras variáveis estão descritas na [documentação de deploy][deploy].

### Para instalação local (sem Docker)

Crie o banco de dados e carregue os dados mínimos necessários:

```
$ make dev_init_db
```

e rode o ambiente de desenvolvimento

```
$ make dev
```

Em poucos segundos você já pode acessar a página no endereço
<http://localhost:8000>.

Na primeira execução (e somente na primeira) é necessário atualizar o índice do
elasticsearch. Para isso, basta executar (com o `make dev` rodando em outro
terminal):

```
$ python radarlegislativo/manage.py rebuild_index
```

Para ver as mudanças no site rapidamente, você pode fazer as alterações
enquanto mantém aberto um terminal com o ``make dev``.

Se você fez mudanças nos arquivos LESS que geram o CSS, você deve
compilar esses arquivos para que as mudanças tenham efeito:

```
$ make compile_css
```

Por favor, mantenha as alterações todas nos arquivos fonte (less)
originais. Todas as alterações feitas diretamente no arquivo
`radar.css` serão sobrescritas antes do deploy.

Se você pretende capturar dados reais do site da Câmara e do Senado, siga
adiante para a [configuração do processo de captura de projetos de
lei][captura].

### Para instalação com Docker

Execute as migrações:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py migrate
```

Depois importe os dados:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py loaddata \
    main/fixtures/tags.json \
    main/fixtures/projetos.json \
    main/fixtures/tramitacoes.json \
    agenda/fixtures/comissoes.json
```

E crie os índices do Elasticsearch:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py rebuild_index
```

Para iniciar a aplicação:

```
$ docker-compose -f docker-compose.dev.yml up
```

### Configuração de palavras chave para definir tema dos filtros

```
$ cp example-palavras_chave.json secrets/palavras_chave.json
```

O arquivo `palavras_chave.json` está em formato para humanos e deve seguir o exemplo.


Produção
--------

O site [Radar Legislativo][website]
ainda está em fase de testes. Caso encontre algum erro, por favor entre em
contato.

[guia-virtualenv]: https://medium.com/@otaviobn/ambiente-virtual-python-com-virtualenv-virtualenvwrapper-no-ubuntu-instala%C3%A7%C3%A3o-e-uso-5e6691b92695
[website]: https://radarlegislativo.org
[django_secret_key]: https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-SECRET_KEY
[telegram_bot]: https://core.telegram.org/bots#6-botfather
[deploy]: https://gitlab.com/codingrights/radarlegislativo/blob/master/docs/deploy.md
[captura]: https://gitlab.com/codingrights/radarlegislativo/blob/master/docs/captura.md
[elasticsearch]: https://www.elastic.co/guide/en/elasticsearch/reference/2.4/index.html
[elastic-install]: https://www.elastic.co/guide/en/elasticsearch/reference/2.4/_installation.html
[less-css]: http://lesscss.org/
[contributing-en]: https://gitlab.com/codingrights/pls/blob/master/CONTRIBUTING.en.md
[docker]: https://docs.docker.com/install/
[docker-compose]: https://docs.docker.com/compose/install/
