Quem contribuiu com o projeto
=============================

- Lucas Teixeira
- [Flávio Amieiro](https://flavioamieiro.com)
- Steffania Paola
- Gabriela Scardine
- [Miguel Soares](https://github.com/oieusouamiguel)
- Giovani Sousa
- Bernardo Fontes
- [Felipe Benites Cabral](https://gitlab.com/felipebcabral)
- [Eduardo Cuducos](https://cuducos.me/)
- [Paula Grangeiro](https://gitlab.com/pgrangeiro)
- [Mariana Bedran Lesche](https://gitlab.com/maribedran)
