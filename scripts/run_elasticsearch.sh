#!/bin/bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"
PARENT_DIR="$(dirname $SCRIPT_DIR)"
CONTRIB_DIR="$PARENT_DIR/contrib"
ELASTIC_BIN="$CONTRIB_DIR/elasticsearch-2.4.6/bin/elasticsearch"

if $(lsof -i:9200 > /dev/null 2>&1)
then
    echo "Elasticsearch já está em execução"
    exit 0
fi

if [ ! -x "$ELASTIC_BIN" ]
then
    echo "Elasticsearch não encontrado em $ELASTIC_BIN."
    echo "Use o script $SCRIPT_DIR/get_elasticsearch.sh para fazer o download."
    exit 1
fi

exec "$ELASTIC_BIN"
