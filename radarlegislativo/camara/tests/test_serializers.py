from unittest import TestCase

from camara import serializers


class SerializeTestCase(TestCase):

    def test_serializes_obj_correctly(self):
        class Stub:

            def __init__(self, attr1, attr2):
                self.attr1 = attr1
                self.attr2 = attr2

            def __getitem__(self, key):
                return getattr(self, key)

        child = Stub(1, 2)
        obj = Stub(child, 4)

        data = serializers.serialize(obj)

        assert 1 == data['attr1']['attr1']
        assert 2 == data['attr1']['attr2']
        assert 4 == data['attr2']
