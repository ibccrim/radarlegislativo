

def serialize(data):
    try:
        dictified = dict(data)
    except TypeError:
        return data
    for key in dictified:
        try:
            dictified[key] = serialize(dictified[key])
        except:
            pass
    return dictified
