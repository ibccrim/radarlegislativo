import requests
import suds.client

from camara import serializers
from camara.exceptions import ClientConnectionError


class BaseClient:

    BASE_PATH = u'http://www.camara.leg.br'
    PATH = ''

    def get_absolute_url(self):
        return '{}/{}'.format(self.BASE_PATH, self.PATH)


class APICamaraClient(BaseClient):

    PATH = u'SitCamaraWS/Proposicoes.asmx?wsdl'

    def get(self, project_id):
        client = suds.client.Client(self.get_absolute_url())
        response = client.service.ObterProposicaoPorID(project_id)
        if 'erro' in response:
            raise ClientConnectionError(u'An error was occoured while trying to get project id: {}'.format(project_id))

        return serializers.serialize(response)


class WebCamaraClient(BaseClient):

    PATH = 'proposicoesWeb/fichadetramitacao'

    def get(self, project_id):
        url = self.get_absolute_url()
        params = {
            'idProposicao': project_id
        }
        response = requests.get(url, params)

        if response.status_code != requests.codes.OK:
            raise ClientConnectionError(u'An error was occoured while trying to get project id: {}'.format(project_id))

        return response
