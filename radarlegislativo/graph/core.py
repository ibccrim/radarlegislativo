#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

Este módulo cria, a partir do nosso banco de dados, uma rede (grafo) ligando projetos de lei a seu/sua autor(a) na Câmara ou no Senado e às categorias as quais eles pertencem. O formato é a estrutura JSON aceita pelo GraphCommons, onde será importado e linkado na página principal.

"""


import uuid
import datetime
import codecs

from main.models import Projeto, Tag


TYPE_IDS = {
    u'apensada': str(uuid.uuid4()),
    u'cadastrado': str(uuid.uuid4()),
    u'foi apensado a': str(uuid.uuid4()),
    u'está em': str(uuid.uuid4()),
    u'tramita em': str(uuid.uuid4()),
    u'foi proposto por': str(uuid.uuid4()),
    u'local': str(uuid.uuid4()),
    u'autoria': str(uuid.uuid4()),
    u'tag': str(uuid.uuid4()),
}

COLOR_PLS = u'#5256A0'
COLOR_LOCAL = u'#5F63BC'
COLOR_AUTORIA = u'#7B99BD'
COLOR_TAG = u'#000000'


def split_apensadas(apensadas):
    return [a.strip() for a in apensadas.split(',')]


def generate_graph():
    graph = {
        'id': u'9a1fa433-0d37-4e6e-a035-90df3d09a7af',
        'name': u'Projetos de Lei que são relevantes para a privacidade, liberdade de expressão, acesso e questões de gênero no meio digital',
        'subtitle': None,
        'description': None,
        'updated_at': str(datetime.datetime.now()),
        'created_at': str(datetime.datetime.now()),
        'status': 0,
        'image': {
         'path': None,
         'ref_name': None,
         'ref_url': None,
        },
        'nodes': [
        ],
        'edges': [
        ],
        'nodeTypes': [
         {
            'id': TYPE_IDS[u'cadastrado'],
            'name': u'Cadastrado',
            'name_alias': None,
            'description': None,
            'image': None,
            'color': COLOR_PLS,
            'image_as_icon': False,
            'properties': [
            ],
            'hide_name': None,
            'size': u'metric_degree',
            'size_limit': 48
         },
         {
            'id': TYPE_IDS[u'local'],
            'name': u'Local',
            'name_alias': None,
            'description': None,
            'image': None,
            'image_as_icon': False,
            'color': COLOR_LOCAL,
            'properties': [
            ],
            'hide_name': None,
            'size': u'metric_degree',
            'size_limit': 48
         },
         {
            'id': TYPE_IDS[u'autoria'],
            'name': u'Autoria',
            'name_alias': None,
            'description': None,
            'image': None,
            'image_as_icon': False,
            'color': COLOR_AUTORIA,
            'properties': [
            ],
            'hide_name': None,
            'size': u'metric_degree',
            'size_limit': 48
         },
         {
            'id': TYPE_IDS[u'tag'],
            'name': u'Tag',
            'name_alias': None,
            'description': None,
            'image': None,
            'image_as_icon': False,
            'color': COLOR_TAG,
            'properties': [
            ],
            'hide_name': None,
            'size': u'metric_degree',
            'size_limit': 48
         },
        ],
        'edgeTypes': [
         {
            'id': TYPE_IDS[u'foi apensado a'],
            'name': u'foi apensado a',
            'name_alias': None,
            'description': None,
            'weighted': 1,
            'directed': 0,
            'durational': None,
            'color': u'#e377c2',
            'properties': [
            ]
         },
         {
            'id': TYPE_IDS[u'está em'],
            'name': u'está em',
            'name_alias': None,
            'description': None,
            'weighted': 1,
            'directed': 1,
            'durational': None,
            'color': u'#777777',
            'properties': [
            ]
         },
         {
            'id': TYPE_IDS[u'tramita em'],
            'name': u'tramita em',
            'name_alias': None,
            'description': None,
            'weighted': 1,
            'directed': 1,
            'durational': None,
            'color': u'#777777',
            'properties': [
            ]
         },
         {
            'id': TYPE_IDS[u'foi proposto por'],
            'name': u'foi proposto por',
            'name_alias': None,
            'description': None,
            'weighted': 1,
            'directed': 1,
            'durational': None,
            'color': u'#be4f5c',
            'properties': [
            ]
         },
        ]
    }

    indice = {}
    autores = set()

    camara = {
        'id': str(uuid.uuid4()),
        'type': u'Local',
        'type_id': TYPE_IDS[u'local'],
        'name': u'Câmara',
        'description': '',
        'color': u'#777777',
        'image': None,
        'reference': None,
        'properties': {
        },
    }
    senado = {
        'id': str(uuid.uuid4()),
        'type': u'Local',
        'type_id': TYPE_IDS[u'local'],
        'name': u'Senado',
        'description': '',
        'color': u'#777777',
        'image': None,
        'reference': None,
        'properties': {
        },
    }
    congresso = {
        'id': str(uuid.uuid4()),
        'type': u'Local',
        'type_id': TYPE_IDS[u'local'],
        'name': u'Congresso',
        'description': '',
        'color': u'#777777',
        'image': None,
        'reference': None,
        'properties': {
        },
    }
    graph['nodes'].extend([camara, senado, congresso])

    tag_index = {}

    for tag in Tag.objects.all():
        node = {
            'id': str(uuid.uuid4()),
            'type': u'Tag',
            'type_id': TYPE_IDS[u'tag'],
            'name': tag.nome,
            'description': '',
            'image': None,
            'reference': None,
            'properties': {
                'id': tag.slug,
            },
        }
        tag_index[tag.slug] = node
        graph['nodes'].append(node)

    sao_cadastrados = set()

    for projeto in Projeto.publicados.all():
        id = str(uuid.uuid4())
        node = {
            'id': id,
            'type': u'Cadastrado',
            'type_id': TYPE_IDS[u'cadastrado'],
            'name': projeto.nome,
            'description': projeto.ementa,
            'image': None,
            'reference': projeto.link,
            'properties': {
                'id': projeto.id,
                'origem': projeto.origem,
                'tags': projeto.tag_list(),
            },
        }

        for tag in projeto.tags.all():
            edge = {
                'to': tag_index[tag.slug]['id'],
                'from': node['id'],
                'name': u'está em',
                'type_id': TYPE_IDS[u'está em'],
                'id': str(uuid.uuid4()),
                'user_id': u'e8bb1f96-5d22-4bc3-96d5-afc6af7940d0',
                'weight': 1,
                'directed': 1,
                'properties': {
                },
            }
            graph['edges'].append(edge)

        sao_cadastrados.add(node['name'])

        autores.add(projeto.autoria)
        indice[projeto.nome] = node
        graph['nodes'].append(node)


    for l in senado, camara:
        edge = {
            'to': l['id'],
            'from': congresso['id'],
            'name': u'está em',
            'type_id': TYPE_IDS[u'está em'],
            'id': str(uuid.uuid4()),
            'user_id': u'e8bb1f96-5d22-4bc3-96d5-afc6af7940d0',
            'weight': 1,
            'directed': 0,
            'properties': {
            },
        }
        graph['edges'].append(edge)

    for node in graph['nodes']:
        if node['type'] == 'Cadastrado':
            if node['properties']['origem'] == u'CA':
                from_ = camara['id']
            else:
                from_ = senado['id']
            edge = {
                'to': node['id'],
                'from': from_,
                'name': u'tramita em',
                'type_id': TYPE_IDS[u'tramita em'],
                'id': str(uuid.uuid4()),
                'user_id': u'e8bb1f96-5d22-4bc3-96d5-afc6af7940d0',
                'weight': 1,
                'directed': 1,
                'properties': {
                },
            }
            graph['edges'].append(edge)

    for autoria in autores:
        node = {
            'id': str(uuid.uuid4()),
            'type': u'Autoria',
            'type_id': TYPE_IDS[u'autoria'],
            'name': autoria,
            'description': '',
            'image': None,
            'reference': None,
            'properties': {
            },
        }
        indice[autoria] = node
        graph['nodes'].append(node)


    done_edges = set()

    for projeto in Projeto.publicados.all():
        for apensada in split_apensadas(projeto.apensadas):
            if apensada in sao_cadastrados:
                id = str(uuid.uuid4())
                edge = {
                    'from': indice[projeto.nome]['id'],
                    'to': indice[apensada]['id'],
                    'name': u'foi apensado a',
                    'type_id': TYPE_IDS[u'foi apensado a'],
                    'id': id,
                    'user_id': u'e8bb1f96-5d22-4bc3-96d5-afc6af7940d0',
                    'weight': 1,
                    'directed': 0,
                    'properties': {
                    },
                }
                index = tuple(sorted((edge['from'], edge['to'])))
                if index not in done_edges:
                    graph['edges'].append(edge)
                done_edges.add(index)

        edge = {
            'from': indice[projeto.nome]['id'],
            'to': indice[projeto.autoria]['id'],
            'name': u'foi proposto por',
            'type_id': TYPE_IDS[u'foi proposto por'],
            'id': str(uuid.uuid4()),
            'user_id': u'e8bb1f96-5d22-4bc3-96d5-afc6af7940d0',
            'weight': 1,
            'directed': 0,
            'properties': {
            },
        }
        index = tuple(sorted((edge['from'], edge['to'])))
        if index not in done_edges:
            graph['edges'].append(edge)
        done_edges.add(index)

    return { 'graph': graph }
