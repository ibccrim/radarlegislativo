# coding: utf-8
from datetime import date
from unittest import TestCase

from main.mappers import CamaraProjectMapper
from main.mappers.camara_mappers import ApensadaMapper, ProposicaoMapper


class ProposicaoMapperTestCase(TestCase):

    def setUp(self):
        self.mapper = ProposicaoMapper

    def test_maps_data_with_nomeProposicao_correctly(self):
        data = {
            'proposicao': {
                'codProposicao': '465004',
                'nomeProposicao': 'PL 6667/2009'
            }
        }

        mapped = list(self.mapper.map(data))
        assert 1 == len(mapped)
        assert 'PL 6667/2009' == mapped[0]

    def test_maps_with_proposicao_correctly(self):
        data = {
            'proposicao': {
                ('nomeProposicao', 'PL 198/2003'): ('codProposicao', '105043'),
                ('nomeProposicao', 'PL 211/2003'): ('codProposicao', '105167'),
                ('nomeProposicao', 'PL 4422/2008'): ('codProposicao', '418562')
            }
        }

        mapped = list(self.mapper.map(data))
        assert 3 == len(mapped)
        assert 'PL 198/2003' in mapped
        assert 'PL 211/2003' in mapped
        assert 'PL 4422/2008' in mapped


class ApensadaMapperTestCase(TestCase):

    def setUp(self):
        self.mapper = ApensadaMapper

    def test_maps_data_with_one_proposicao_correctly(self):
        data = {
            'Apreciacao': u'Proposição Sujeita à Apreciação Conclusiva pelas Comissões - Art. 24 II',
            'Autor': u'Cláudio Magrão',
            'DataApresentacao': u'11/08/2004',
            'Ementa': u'Dispõe sobre os limites à concentração econômica nos meios de comunicação social, e dá outras providências.',
            'ExplicacaoEmenta': u'Altera o Decreto-Lei nº 236, de 1967.',
            'Indexacao': u'Limitação, concentração, poder econômico, meios de comunicação, propriedade, emissora, rádio, televisão, exigência, concessão, permissão, limite máximo, quantidade, audiência, normas, transmissão, programação, proibição, transferência, direitos, ausência, autorização, Poder Concedente, penalidade, infrator._ Revogação, dispositivos, decreto-lei federal, alteração, Código Brasileiro de Telecomunicações, limitação, quantidade, propriedade, concessão, permissão, emissora, rádio, televisão.',
            'LinkInteiroTeor': u'http://www.camara.gov.br/proposicoesWeb/prop_mostrarintegra?codteor=236118',
            'RegimeTramitacao': u'Ordinária (Art. 151, III, RICD)',
            'Situacao': u'CCTCI - Pronta para Pauta',
            'UltimoDespacho': {
                '_Data': u'22/06/2015',
                'value': u'Defiro o Requerimento n. 2.093/2015, nos termos do art. 141 do Regimento Interno da Câmara dos Deputados - RICD. Revejo o despacho inicial aposto ao Projeto de Lei n. 4.026/2004, para incluir o exame de mérito pela Comissão de Desenvolvimento Econômico, Indústria e Comércio. Esclareço que, para os fins do art. 191, III, do RICD, prevalecerá a ordem de distribuição prevista neste despacho. Publique-se. Oficie-se. [ATUALIZAÇÃO DO DESPACHO DO PL n. 4.026/2004: À CDEIC, CCTCI e CCJC (art. 54 do RICD) - Proposição sujeita à apreciação conclusiva pelas comissões - art. 24, II, do RICD. Regime de tramitação: Ordinária.]'
            },
            '_ano': '2004',
            '_numero': '4026',
            '_tipo': 'PL',
            'apensadas': {
                'proposicao': {
                    'codProposicao': u'465004',
                    'nomeProposicao': 'PL 6667/2009'
                }
            },
            'idProposicao': '261833',
            'idProposicaoPrincipal': {},
            'ideCadastro': '74263',
            'nomeProposicao': u'PL 4026/2004',
            'nomeProposicaoOrigem': {},
            'partidoAutor': u'PPS',
            'tema': u'Comunicações',
            'tipoProposicao': u'Projeto de Lei',
            'ufAutor': 'SP'
        }

        mapped = self.mapper.map(data)
        assert 'PL 6667/2009' == mapped

    def test_maps_data_with_many_proposicao_correctly(self):
        data = {
            'Apreciacao': u'Proposição Sujeita à Apreciação Conclusiva pelas Comissões - Art. 24 II',
            'Autor': u'Cláudio Magrão',
            'DataApresentacao': u'11/08/2004',
            'Ementa': u'Dispõe sobre os limites à concentração econômica nos meios de comunicação social, e dá outras providências.',
            'ExplicacaoEmenta': u'Altera o Decreto-Lei nº 236, de 1967.',
            'Indexacao': u'Limitação, concentração, poder econômico, meios de comunicação, propriedade, emissora, rádio, televisão, exigência, concessão, permissão, limite máximo, quantidade, audiência, normas, transmissão, programação, proibição, transferência, direitos, ausência, autorização, Poder Concedente, penalidade, infrator._ Revogação, dispositivos, decreto-lei federal, alteração, Código Brasileiro de Telecomunicações, limitação, quantidade, propriedade, concessão, permissão, emissora, rádio, televisão.',
            'LinkInteiroTeor': u'http://www.camara.gov.br/proposicoesWeb/prop_mostrarintegra?codteor=236118',
            'RegimeTramitacao': u'Ordinária (Art. 151, III, RICD)',
            'Situacao': u'CCTCI - Pronta para Pauta',
            'UltimoDespacho': {
                '_Data': u'22/06/2015',
                'value': u'Defiro o Requerimento n. 2.093/2015, nos termos do art. 141 do Regimento Interno da Câmara dos Deputados - RICD. Revejo o despacho inicial aposto ao Projeto de Lei n. 4.026/2004, para incluir o exame de mérito pela Comissão de Desenvolvimento Econômico, Indústria e Comércio. Esclareço que, para os fins do art. 191, III, do RICD, prevalecerá a ordem de distribuição prevista neste despacho. Publique-se. Oficie-se. [ATUALIZAÇÃO DO DESPACHO DO PL n. 4.026/2004: À CDEIC, CCTCI e CCJC (art. 54 do RICD) - Proposição sujeita à apreciação conclusiva pelas comissões - art. 24, II, do RICD. Regime de tramitação: Ordinária.]'
            },
            '_ano': '2004',
            '_numero': '4026',
            '_tipo': 'PL',
            'apensadas': {
                'proposicao': {
                    ('nomeProposicao', 'PL 198/2003'): ('codProposicao', '105043'),
                    ('nomeProposicao', 'PL 211/2003'): ('codProposicao', '105167'),
                    ('nomeProposicao', 'PL 4422/2008'): ('codProposicao', '418562')
                }
            },
            'idProposicao': '261833',
            'idProposicaoPrincipal': {},
            'ideCadastro': '74263',
            'nomeProposicao': u'PL 4026/2004',
            'nomeProposicaoOrigem': {},
            'partidoAutor': u'PPS',
            'tema': u'Comunicações',
            'tipoProposicao': u'Projeto de Lei',
            'ufAutor': 'SP'
        }

        mapped = self.mapper.map(data)
        assert 'PL 198/2003' in mapped
        assert 'PL 211/2003' in mapped
        assert 'PL 4422/2008' in mapped

    def test_maps_data_without_proposicao_correctly(self):
        data = {
            'Apreciacao': u'Proposição Sujeita à Apreciação Conclusiva pelas Comissões - Art. 24 II',
            'Autor': u'Cláudio Magrão',
            'DataApresentacao': u'11/08/2004',
            'Ementa': u'Dispõe sobre os limites à concentração econômica nos meios de comunicação social, e dá outras providências.',
            'ExplicacaoEmenta': u'Altera o Decreto-Lei nº 236, de 1967.',
            'Indexacao': u'Limitação, concentração, poder econômico, meios de comunicação, propriedade, emissora, rádio, televisão, exigência, concessão, permissão, limite máximo, quantidade, audiência, normas, transmissão, programação, proibição, transferência, direitos, ausência, autorização, Poder Concedente, penalidade, infrator._ Revogação, dispositivos, decreto-lei federal, alteração, Código Brasileiro de Telecomunicações, limitação, quantidade, propriedade, concessão, permissão, emissora, rádio, televisão.',
            'LinkInteiroTeor': u'http://www.camara.gov.br/proposicoesWeb/prop_mostrarintegra?codteor=236118',
            'RegimeTramitacao': u'Ordinária (Art. 151, III, RICD)',
            'Situacao': u'CCTCI - Pronta para Pauta',
            'UltimoDespacho': {
                '_Data': u'22/06/2015',
                'value': u'Defiro o Requerimento n. 2.093/2015, nos termos do art. 141 do Regimento Interno da Câmara dos Deputados - RICD. Revejo o despacho inicial aposto ao Projeto de Lei n. 4.026/2004, para incluir o exame de mérito pela Comissão de Desenvolvimento Econômico, Indústria e Comércio. Esclareço que, para os fins do art. 191, III, do RICD, prevalecerá a ordem de distribuição prevista neste despacho. Publique-se. Oficie-se. [ATUALIZAÇÃO DO DESPACHO DO PL n. 4.026/2004: À CDEIC, CCTCI e CCJC (art. 54 do RICD) - Proposição sujeita à apreciação conclusiva pelas comissões - art. 24, II, do RICD. Regime de tramitação: Ordinária.]'
            },
            '_ano': '2004',
            '_numero': '4026',
            '_tipo': 'PL',
            'apensadas': {},
            'idProposicao': '261833',
            'idProposicaoPrincipal': {},
            'ideCadastro': '74263',
            'nomeProposicao': 'PL 4026/2004',
            'nomeProposicaoOrigem': {},
            'partidoAutor': u'PPS',
            'tema': u'Comunicações',
            'tipoProposicao': u'Projeto de Lei',
            'ufAutor': 'SP'
        }

        mapped = self.mapper.map(data)
        assert '' == mapped


class CamaraProjectMapperTestCase(TestCase):

    def setUp(self):
        self.mapper = CamaraProjectMapper

    def test_maps_data_correctly(self):
        data = {
            'proposicao': {
                'Apreciacao': u'Proposição Sujeita à Apreciação Conclusiva pelas Comissões - Art. 24 II',
                'Autor': 'Cl\xc3\xa1udio Magr\xc3\xa3o',
                'DataApresentacao': '11/08/2004',
                'Ementa': 'Disp\xc3\xb5e sobre os limites \xc3\xa0 concentra\xc3\xa7\xc3\xa3o econ\xc3\xb4mica nos meios de comunica\xc3\xa7\xc3\xa3o social, e d\xc3\xa1 outras provid\xc3\xaancias.',
                'ExplicacaoEmenta': u'Altera o Decreto-Lei nº 236, de 1967.',
                'Indexacao': u'Limitação, concentração, poder econômico, meios de comunicação, propriedade, emissora, rádio, televisão, exigência, concessão, permissão, limite máximo, quantidade, audiência, normas, transmissão, programação, proibição, transferência, direitos, ausência, autorização, Poder Concedente, penalidade, infrator._ Revogação, dispositivos, decreto-lei federal, alteração, Código Brasileiro de Telecomunicações, limitação, quantidade, propriedade, concessão, permissão, emissora, rádio, televisão.',
                'LinkInteiroTeor': u'http://www.camara.gov.br/proposicoesWeb/prop_mostrarintegra?codteor=236118',
                'RegimeTramitacao': u'Ordinária (Art. 151, III, RICD)',
                'Situacao': u'CCTCI - Pronta para Pauta',
                'UltimoDespacho': {
                    '_Data': u'22/06/2015',
                    'value': u'Defiro o Requerimento n. 2.093/2015, nos termos do art. 141 do Regimento Interno da Câmara dos Deputados - RICD. Revejo o despacho inicial aposto ao Projeto de Lei n. 4.026/2004, para incluir o exame de mérito pela Comissão de Desenvolvimento Econômico, Indústria e Comércio. Esclareço que, para os fins do art. 191, III, do RICD, prevalecerá a ordem de distribuição prevista neste despacho. Publique-se. Oficie-se. [ATUALIZAÇÃO DO DESPACHO DO PL n. 4.026/2004: À CDEIC, CCTCI e CCJC (art. 54 do RICD) - Proposição sujeita à apreciação conclusiva pelas comissões - art. 24, II, do RICD. Regime de tramitação: Ordinária.]'
                },
                '_ano': '2004',
                '_numero': '4026',
                '_tipo': 'PL',
                'apensadas': {
                    'proposicao': {
                        ('nomeProposicao', 'PL 198/2003'): ('codProposicao', '105043'),
                        ('nomeProposicao', 'PL 211/2003'): ('codProposicao', '105167'),
                        ('nomeProposicao', 'PL 4422/2008'): ('codProposicao', '418562')
                    }
                },
                'idProposicao': '261833',
                'idProposicaoPrincipal': {},
                'ideCadastro': '74263',
                'nomeProposicao': 'PL 4026/2004',
                'nomeProposicaoOrigem': {},
                'partidoAutor': u'PPS',
                'tema': u'Comunicações',
                'tipoProposicao': u'Projeto de Lei',
                'ufAutor': 'SP'
            }
        }

        mapped = self.mapper.map(data)

        assert 'PL 4026/2004' == mapped['nome']
        assert u'Cláudio Magrão' == mapped['autoria']
        assert u'Dispõe sobre os limites à concentração econômica nos meios de comunicação social, e dá outras providências.' == mapped['ementa']
        assert date(2004, 8, 11) == mapped['apresentacao'].date()
        assert 'America/Sao_Paulo' == str(mapped['apresentacao'].tzinfo)

        assert 'PL 198/2003' in mapped['apensadas']
        assert 'PL 211/2003' in mapped['apensadas']
        assert 'PL 4422/2008' in mapped['apensadas']
