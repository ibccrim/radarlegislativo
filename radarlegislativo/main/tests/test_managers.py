# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
from django.test import TestCase, mock
from freezegun import freeze_time
from model_mommy import mommy

from main.models import Projeto


__all__ = ["TesteProjetoManager"]


class TesteProjetoManager(TestCase):

    @freeze_time('2018-04-05')
    def setUp(self):
        self.pl_novo = mommy.make('Projeto', publicado=True)

        # Precisamos criar os objetos e depois mudar a data de
        # cadastro manualmente por que o auto_now_add sempre
        # sobrescreve o valor que definimos.
        self.pl_antigo = mommy.make('Projeto', publicado=True)
        self.pl_antigo.cadastro = datetime.date(2018, 3, 31)
        self.pl_antigo.save()

        mommy.make(Projeto, publicado=False)

    def teste_encontra_o_PL_do_dia_05_na_mesma_semana_que_o_dia_06(self):
        self.assertIn(
            self.pl_novo,
            Projeto.publicados.na_mesma_semana_que_o_dia(
                datetime.date(2018, 4, 6)),
        )

    def teste_nao_encontra_o_PL_do_dia_31_na_mesma_semana_que_o_dia_06(self):
        self.assertNotIn(
            self.pl_antigo,
            Projeto.publicados.na_mesma_semana_que_o_dia(
                datetime.date(2018, 4, 6)),
        )

    @mock.patch('main.models.ProjetoManager.na_mesma_semana_que_o_dia')
    def teste_novos_PLs_mostra_a_semana_do_mais_novo(self, filtro_mockado):
        Projeto.publicados.novos()
        filtro_mockado.assert_called_once_with(self.pl_novo.cadastro)

    def test_encontra_PL_mais_novo(self):
        self.assertEqual(Projeto.publicados.mais_novo(), self.pl_novo)

    def test_manager_objects(self):
         self.assertEqual(3, Projeto.objects.count())

    def test_manager_publicados(self):
         self.assertEqual(2, Projeto.publicados.count())

    def test_manager_nao_publicados(self):
         self.assertEqual(1, Projeto.nao_publicados.count())
