# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import re
import urllib
import xml.etree.ElementTree as ET
from hashlib import md5

import suds.client
from bs4 import BeautifulSoup
from pytz import timezone

from .models import Projeto, Tramitacao
from .models import URL_API_CAMARA, URL_WEB_CAMARA
from .models import URL_API_SENADO_MATERIA, URL_API_SENADO_MOVIMENTACOES


def conv(string):
    try:
        return string.encode('latin-1').decode('latin-1')
    except:
        try:
            return string.encode('utf-8').decode('utf-8')
        except:
            return string


def dictify(data):
    try:
        dictified = dict(data)
    except:
        return data
    for k in dictified.keys():
        try:
            dictified[k] = _dictify(dictified[k])
        except:
            pass
    return dictified


class FetcherError(Exception):
    pass


def validate_fetcher_kwargs(kwargs):
    valid_kwargs= {'tags', 'fetch_apensadas', 'save', 'publish'}
    kwargs_keys = set(kwargs.keys())
    if not kwargs_keys.issubset(valid_kwargs):
        invalid = ', '.join(kwargs_keys - valid_kwargs)
        raise FetcherError(u"{} são kwargs inválidos".format(invalid))

    tags = kwargs.get('tags', [])
    fetch_apensadas = kwargs.get('fetch_apensadas', False)
    save = kwargs.get('save', True)
    publish = kwargs.get('publish', True)
    return tags, fetch_apensadas, save, publish


# Senado

def get_apensadas_from_senado_xml_tree(tree):
    apensadas = []
    relacionadas = tree.find('.//MateriasAnexadas')
    if relacionadas is not None:
        for r in list(relacionadas):
            nome = conv(r.find('.//SiglaSubtipoMateria').text) + \
                    ' ' + conv(r.find('.//NumeroMateria').text.lstrip('0'))
            apensadas.append(nome)
    return ", ".join(apensadas)


def fetch_tramitacoes_for_senado_project(projeto):
    def get_tag_content(caminho, default_value=''):
        try:
            return conv(tree.find(caminho).text)
        except AttributeError:
            return default_value

    xmlfile = urllib.urlopen(
        URL_API_SENADO_MOVIMENTACOES.format(projeto.id_site))
    contents = xmlfile.read()
    tree = ET.fromstring(contents)

    for tree_tramitacao in tree.find('.//Tramitacoes'):
        id_tramitacao = conv(get_tag_content('.//CodigoTramitacao'))
        texto = conv(get_tag_content('.//TextoTramitacao'))
        texto = re.sub('\s\s*', ' ', texto.strip())
        tree_situacao = tree_tramitacao.find('.//Situacao/DescricaoSituacao')
        if tree_situacao is not None:
            situacao = conv(tree_situacao.text)
            situacao_stripped = re.sub('\s\s*', ' ', situacao.strip())
            descricao = u'{} — {}'.format(situacao_stripped, texto)
        else:
            descricao = texto
        data_str = conv(get_tag_content('.//DataTramitacao'))
        data = datetime.datetime(*map(int, data_str.split('-'))). \
            replace(tzinfo=timezone('America/Sao_Paulo'))
        local_ = conv(get_tag_content('.//NomeLocal'))
        local_stripped = re.sub('\s\s*', ' ', local_.strip())
        if len(local_stripped) > 255:
            local_stripped = local_stripped[:255]
        defaults = {
            "data": data,
            "projeto": projeto,
            "local": local_stripped,
            "descricao": descricao,
        }
        tramitacao, created = Tramitacao.objects.get_or_create(
                id_site=id_tramitacao, defaults=defaults)


def fetch_senado_project(id_senado, **kwargs):
    tags, fetch_apensadas, save, publish = validate_fetcher_kwargs(kwargs)

    def get_tag_content(caminho, default_value=None):
        try:
            return conv(tree.find(caminho).text)
        except AttributeError:
            return default_value

    xmlfile = urllib.urlopen(URL_API_SENADO_MATERIA.format(id_senado))
    contents = xmlfile.read()
    tree = ET.fromstring(contents)

    if tree.find(".//DataApresentacao") is None:
        raise FetcherError((u"Erro ao coletar informações no Senado "
                            u"para projeto com id {}").format(id_senado))

    new_values = {}

    new_values["html_original"] = contents
    new_values["nome"] = get_tag_content('.//SiglaSubtipoMateria', '') + ' ' + get_tag_content('.//NumeroMateria', '').lstrip('0')
    new_values["apresentacao"] = datetime.datetime(*map(int, get_tag_content('.//DataApresentacao', '').split('-'))
                                ).replace(tzinfo=timezone('America/Sao_Paulo'))
    new_values["ementa"] = get_tag_content('.//EmentaMateria', '')
    new_values["autoria"] = get_tag_content('.//SiglaTipoAutor', '') + ' ' + get_tag_content('.//NomeAutor', '')
    new_values["apensadas"] = get_apensadas_from_senado_xml_tree(tree)
    new_values["local"] = get_tag_content('.//NomeLocal')
    new_values["publicado"] = publish

    if save:
        projeto, created = Projeto.objects.update_or_create(
            id_site=id_senado,
            origem=Projeto.SENADO,
            defaults=new_values,
        )

        # Para usar o relacionamento ManyToMany, o objeto precisa ter
        # um id.  Por isso a atualização das tags fica aqui no final,
        # depois que ele foi salvo uma primeira vez (mesmo quando ele
        # está sendo criado).
        projeto.tags.set(tags)

        fetch_tramitacoes_for_senado_project(projeto)
    else:
        try:
            projeto = Projeto.objects.get(origem=Projeto.SENADO,
                                          id_site=id_senado)
        except Projeto.DoesNotExist:
            projeto = Projeto(origem=Projeto.SENADO, id_site=id_senado)

        for attr, val in new_values.items():
            setattr(projeto, attr, val)

    return projeto


# Câmara

def get_apensadas_from_camara_project(projeto_as_dict):
    apensadas = []
    apensadas_soap = projeto_as_dict.get('apensadas', {})

    if apensadas_soap:
        if 'nomeProposicao' in apensadas_soap['proposicao']:
            nome = conv(apensadas_soap['proposicao']['nomeProposicao'])
            apensadas.append(nome)
        else:
            if type(apensadas_soap['proposicao']) == list:
                for apensada in apensadas_soap['proposicao']:
                    apensadas.append(conv(apensada['nomeProposicao']))
            else:
                for apensada in apensadas_soap['proposicao'].items():
                    apensadas.append(apensada[1])

    return ", ".join(apensadas)


def scrape_tramitacoes_for_camara_project(projeto):
    html = projeto.html_original
    soup = BeautifulSoup(html, 'html.parser')
    tramitacoes = soup.find(attrs={"id": "tramitacoes"})

    tds = list(tramitacoes.find('table').find_all('td'))
    it = iter(tds)
    zipped = zip(it, it)[::-1]

    for data_element, texto in zipped:
        data_str = conv(data_element.text.strip())
        data = datetime.datetime(*map(int, data_str.split('/')[::-1])). \
            replace(tzinfo=timezone('America/Sao_Paulo'))

        local_ = conv(texto.find(attrs={'class': 'paragrafoTabelaTramitacoes'}).text.strip())
        local_ = re.sub('\s\s*', ' ', local_)
        if len(local_) > 255:
            local_ = local_[:255]

        descricao = conv(texto.find(attrs={'class': 'ulTabelaTramitacoes'}).text.strip())
        descricao = re.sub('\s\s*', ' ', descricao)

        digest_input = data.strftime("%Y-%m-%d") + local_ + descricao
        id_tramitacao = md5(digest_input.encode('utf-8')).hexdigest(),

        defaults = {
            "data": data,
            "projeto": projeto,
            "local": local_,
            "descricao": descricao,
        }
        tramitacao, created = Tramitacao.objects.get_or_create(
                id_site=id_tramitacao, defaults=defaults)


def encontrar_local_na_camara(html):
    soup = BeautifulSoup(html, 'html.parser')
    table = soup.find(attrs={"id": "pareceresValidos"})
    if table is not None:
        table.findChild('td')
        text = table.get_text()
        return re.sub('\s+', ' ', text).strip()
    else:
        return ""


def fetch_camara_project(id_camara, **kwargs):
    tags, fetch_apensadas, save, publish = validate_fetcher_kwargs(kwargs)

    soap_client = suds.client.Client(URL_API_CAMARA)
    soap_object = soap_client.service.ObterProposicaoPorID(id_camara)

    if 'erro' in soap_object:
        raise FetcherError((u"Erro ao coletar informações na câmara "
                            u"para projetocom id {}").format(id_camara))

    projeto_as_dict = dictify(soap_object['proposicao'])

    new_values = {}

    new_values["html_original"] = urllib.urlopen(URL_WEB_CAMARA.format(id_camara)).read()

    new_values["nome"] = conv(projeto_as_dict.get('nomeProposicao', ''))
    new_values["autoria"] = conv(projeto_as_dict.get('Autor', ''))
    if len(new_values["autoria"]) > 250:
        new_values["autoria"] = new_values["autoria"][:250] + '...'

    apresentacao_str = conv(projeto_as_dict.get('DataApresentacao', ''))
    new_values["apresentacao"] = datetime.datetime(*map(int, apresentacao_str.split('/')[::-1])). \
        replace(tzinfo=timezone('America/Sao_Paulo'))
    new_values["ementa"] = conv(projeto_as_dict.get('Ementa', ''))
    new_values["apensadas"] = get_apensadas_from_camara_project(projeto_as_dict)
    new_values["local"] = encontrar_local_na_camara(new_values["html_original"])
    new_values["publicado"] = publish

    if save:
        projeto, created = Projeto.objects.update_or_create(
            id_site=id_camara,
            origem=Projeto.CAMARA,
            defaults=new_values,
        )

        # Para usar o relacionamento ManyToMany, o objeto precisa ter
        # um id.  Por isso a atualização das tags fica aqui no final,
        # depois que ele foi salvo uma primeira vez (mesmo quando ele
        # está sendo criado).
        projeto.tags.set(tags)

        scrape_tramitacoes_for_camara_project(projeto)

    else:
        try:
            projeto = Projeto.objects.get(
                origem=Projeto.CAMARA, id_site=id_camara)
        except Projeto.DoesNotExist:
            projeto = Projeto(origem=Projeto.CAMARA, id_site=id_camara)

        for attr, val in new_values.items():
            setattr(projeto, attr, val)

    return projeto
