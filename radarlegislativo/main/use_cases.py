from main.models import PalavraChave, Projeto, Tag, Tramitacao
from main.services import fetch_camara_project, fetch_senado_project


class BaseFetchProjects:

    def get_service(self):
        raise NotImplementedError

    def get_source(self):
        raise NotImplementedError

    def execute(self, external_id, tag_ids=None, keywords_ids=None, previously_created=False, publish=True):
        service = self.get_service()
        source = self.get_source()
        tag_ids = tag_ids or []
        keywords_ids = keywords_ids or []

        kwargs = service(external_id)
        tramitacoes = kwargs.pop('tramitacoes', [])
        kwargs['publicado'] = publish

        instance, created = Projeto.objects.update_or_create(
            origem=source, id_site=external_id, defaults=kwargs)

        if not previously_created or created:
            for tag in Tag.objects.filter(id__in=tag_ids):
                instance.tags.add(tag)

            for keyword in PalavraChave.objects.filter(id__in=keywords_ids):
                instance.palavras_chave.add(keyword)

            for tramitacao in tramitacoes:
                tramitacao['projeto'] = instance
                id_site = tramitacao.pop('id_site')
                Tramitacao.objects.get_or_create(
                    id_site=id_site,
                    defaults=tramitacao,
                )


class FetchProjectsFromSenado(BaseFetchProjects):

    def get_service(self):
        return fetch_senado_project

    def get_source(self):
        return Projeto.SENADO


class FetchProjectsFromCamara(BaseFetchProjects):

    def get_service(self):
        return fetch_camara_project

    def get_source(self):
        return Projeto.CAMARA
