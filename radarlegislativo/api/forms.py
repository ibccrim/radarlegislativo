# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.conf import settings

from main.models import PalavraChave, Projeto, Tag


class TokenField(forms.CharField):

    def validate(self, value):
        if value != settings.RASPADOR_API_TOKEN:
            raise forms.ValidationError("Token inválido")


class TagsField(forms.CharField):

    @staticmethod
    def _palavra_chave(nome):
        nome = nome.strip()
        if not nome:
            return

        obj, _ = PalavraChave.objects.get_or_create(
            nome=nome,
            defaults={'nome': nome}
        )
        return obj

    def to_python(self, value):
        """Transforma as palavras-chave vindas do Raspador Legislativo em uma
        sequência de `PalavraChave` e uma sequência de `Tag`"""
        if not value:
            raise forms.ValidationError("Palavras-chave são obrigatórias")

        keywords = (self._palavra_chave(v) for v in value.split(','))
        keywords = tuple(k for k in keywords if k)

        tag_names = (
            settings.PALAVRAS_CHAVE.get(keyword.nome)
            for keyword in keywords
        )
        tag_names_cleaned = set(name for name in tag_names if name)

        if not tag_names_cleaned:
            return keywords, tuple()

        qs = Tag.objects.filter(nome__in=tag_names_cleaned)
        if not qs.exists():
            return keywords, tuple()

        return keywords, tuple(tag for tag in qs)


class FormularioProjeto(forms.ModelForm):
    token = TokenField()
    palavras_chave = TagsField(required=False)

    class Meta(object):
        model = Projeto
        fields = ("id_site", "origem")
