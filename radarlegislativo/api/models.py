#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class UltimaNotificacao(models.Model):
    enviada_em = models.DateTimeField('última notificação', auto_now=True)
    assunto = models.CharField(max_length=255)
    destinatarios = models.TextField('emails')
    corpo = models.TextField()

    def __str__(self):
        return '{} <{}>'.format(self.assunto, self.enviada_em)

    def full_clean(self):
        if UltimaNotificacao.objects.exists() and not self.pk:
            raise ValidationError((
                u'Só pode haver um objeto "Notificacao". '
                u'Edite o que já existe.'
            ))

        self.destinatarios = ', '.join(self.destinatarios)
        return super(UltimaNotificacao, self).full_clean()


@receiver(pre_save, sender=UltimaNotificacao)
def valida_ultima_notificacao(sender, instance, raw, using, **kwargs):
    if UltimaNotificacao.objects.exists() and not instance.pk:
        raise ValidationError((
            u'Só pode haver um objeto "Notificacao". '
            u'Edite o que já existe.'
        ))
    instance.destinatarios = ', '.join(instance.destinatarios)
    return instance
