from django.core.cache import cache

from website.use_cases import UpdateCachedTags


class CachedTagsMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        if not cache.get('tags'):
            UpdateCachedTags().execute()

        response = self.get_response(request)
        return response
